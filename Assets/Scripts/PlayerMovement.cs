﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {
	public Text healthText;
	public float speed = 1f;
	public float max=2f;
	public AudioClip rocketExplode;
	
	private int healthPerSoda = 1;
	private int healthPerFruit = 2;
	private Animator animator;
	public int playerHealth = 5;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		healthText.text = "Health: " + playerHealth;
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKey (KeyCode.D))
			transform.position += new Vector3 (speed * Time.deltaTime, 0.0f, 0.0f);
		if(Input.GetKey (KeyCode.A))
			transform.position -= new Vector3 (speed * Time.deltaTime, 0.0f, 0.0f);
		if (Input.GetKey (KeyCode.W))
			transform.position += new Vector3 (0.0f, speed * Time.deltaTime, 0.0f);
		if (Input.GetKey (KeyCode.S))
			transform.position -= new Vector3 (0.0f, speed * Time.deltaTime, 0.0f);
	}
	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		if(objectPlayerCollidedWith.tag == "Soda")
		{
			playerHealth += healthPerSoda;
			healthText.text = "+" + healthPerSoda + "Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(rocketExplode);
		}
		else if(objectPlayerCollidedWith.tag == "Fruit")
		{
			playerHealth += healthPerFruit;
			healthText.text = "+" + healthPerFruit + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(rocketExplode);
		}
	}
	public void TakeDamage(int damageReceived)
	{
		playerHealth -= damageReceived;
		healthText.text = "-" + damageReceived + " Health\n" + "Health: " + playerHealth;
		Debug.Log("Player Health: " + playerHealth);
	}
}