﻿using UnityEngine;
using System.Collections;

public class Enemy : MovingObject {
	public float min=1f;
	public float max=2f;
	public int attackDamage;
	public bool isEnemyStrong;

	private Animator Animator;
	private Transform player;

	// Use this for initialization
	protected void Start () {
		
		min=transform.position.x;
		max=transform.position.x+3;
	}
	// Update is called once per frame
	void Update () {
		
		
		transform.position =new Vector3(Mathf.PingPong(Time.time*2,max-min)+min, transform.position.y, transform.position.z);
		
	}
	void OnCollisionEnter(Collision col)
	{
	if(col.gameObject.tag == "Player")
			Debug.Log("boop");
	}
     void HandleCollision<T>(T Component)
	{
		PlayerMovement Player = Component as PlayerMovement;
		Player.TakeDamage(attackDamage);
		Animator.SetTrigger("enemyAttack");
	}
}